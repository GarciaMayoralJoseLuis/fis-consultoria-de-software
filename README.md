# Fis Consultoria De Software


## La consultoría
```plantuml
@startmindmap
*[#PaleVioletRed] La consultoría
	*[#OliveDrab] ¿Qué es la consultoría?
		*[#LightGreen] Nace a partir de las necesidades de las grandes empresas
			*[#SkyBlue] Necesidad de evolucionar
			*[#SkyBlue] La gran complejidad interna
			*[#SkyBlue] Dado que las empresas no \npueden operar y transformarse al mismo tiempo
				*[#GreenYellow] Esto es debido a
					*[#SkyBlue] *Falta de personal cualificado
					*[#SkyBlue] *Falta de conocimiento específico
					*[#SkyBlue] *Falta de tiempo
		*[#LightGreen] Consulting
			*[#SkyBlue] Empresas consultoras
				*[#GreenYellow] Se caracterizan por 
					*[#SkyBlue] *Conocimiento sectorial
					*[#SkyBlue] *Conocimiento técnico
					*[#SkyBlue] *Capacidad de plantear soluciones 
				*[#GreenYellow] Un ejemplo
					*[#SkyBlue] Apex Consulting
						*[#LightGreen] Descripción
							*[#GreenYellow] Compañía multinacional española \nde consultoría  y tecnología de \ninformación.
							*[#GreenYellow] Tiene presencia en países como 
								*[#SkyBlue] *En Europa: España, Francia, UK
								*[#SkyBlue] *En LATAM: México, Perú, Colombia,\nPanamá, República Dominicana y Argentina
	*[#OliveDrab] Servicios que presta
		*[#LightGreen] Consultoría
			*[#SkyBlue] Se compone por 
				*[#SkyBlue] Reingeniería de procesos
					*[#GreenYellow] Organización actividades y procesos de la \nempresa de acuerdo a un esquema más óptimo que el actual.
				*[#SkyBlue] Gobierno TI
					*[#GreenYellow] Se centra en consultorías al CEO de una empresa
					*[#GreenYellow] Promueve prácticas de
						*[#SkyBlue] Calidad
						*[#SkyBlue] Mitologías de desarrollo 
						*[#SkyBlue] Arquitectura empresarial
				*[#SkyBlue] Oficina de proyectos
					*[#GreenYellow] Se centra en la gestión de proyectos \norganizacionales
				*[#SkyBlue] Analítica avanzada 
					*[#GreenYellow] Dada la importancia de la información. \nEste se centra en el manejo y obtención de información
		*[#LightGreen] Integración
			*[#SkyBlue] Toma como base el mapa de sistemas \nde le empresa y mejorarlo
				*[#SkyBlue] Dicho mapa, necesita una constante \nevolución, aquí entran las consultoras, cuenta con:
					*[#GreenYellow] *Piezas software
					*[#GreenYellow] *Piezas hardware
			*[#SkyBlue] Se compone por 
				*[#SkyBlue] Desarrollos a medida
					*[#GreenYellow] Integración de nuevas tecnologías y diferentes lenguajes		
				*[#SkyBlue] Aseguramiento de la calidad E2E
					*[#GreenYellow] Calidad de software 
				*[#SkyBlue] Infraestructuras
					*[#GreenYellow] Implementación de diversas tecnologías
						*[#SkyBlue] * Software prediseñado y parametrizado 
						*[#SkyBlue] * Plataformas en la nube, etc
				*[#SkyBlue] Soluciones de mercado
					*[#GreenYellow] En base a la demande, proporciona \nsoluciones y las adapta
		*[#LightGreen] Externalización
			*[#SkyBlue] Representa el 50% de la demande de la consultaría
			*[#SkyBlue] Consiste en definir organizaciones de servicios \na largo plazo para asumir actividades que no son a fin a las empresas cliente.
				*[#GreenYellow] Gestión de aplicaciones
					*[#SkyBlue] Se delegan los servicios de mantenimiento y evolución
				*[#GreenYellow] Servicios SQA
					*[#SkyBlue] Realiza pruebas de aplicaciones
				*[#GreenYellow] Operaciones y administración de infraestructura
					*[#SkyBlue] Aquí se externalizan operaciones de negocio
				*[#GreenYellow] Procesos de negocio
 
	*[#OliveDrab] ¿por qué es una profesión de futuro?
		*[#LightGreen] Papel en el ambito laboral
			*[#SkyBlue] Tiene el papel de proveer  servicios de \nhabilitación digital. E impulsa diversas tendencias como:
				*[#GreenYellow] *Transformación digital
				*[#GreenYellow] *Idusctria 4.0
				*[#GreenYellow] *Smart Cities

		*[#LightGreen] En la sociedad del conocimiento, la consultoría, \nes un modo de vida, que permite alcanzar la \nfelicidad profesional
			*[#SkyBlue] Como profesión
				*[#GreenYellow] Se categoriza en base a la expericia
					*[#SkyBlue] Junior
						*[#LightGreen] Asistente
						*[#LightGreen] Consultor
					*[#SkyBlue] Senior
						*[#LightGreen] Jefe de equipo
						*[#LightGreen] Jefe de proyecto
					*[#SkyBlue] Gerente
					*[#SkyBlue] Director
			*[#SkyBlue] Nivel de exigencia
				*[#GreenYellow] Necesidad de profesionelaes capaces 
					*[#SkyBlue] por su:
						*[#LightGreen] *Formación y experiencia
						*[#LightGreen] *Capacidad de trabajo y evolución
					*[#SkyBlue] Capaces de:
						*[#LightGreen] *Colaborar en un proyecto común
						*[#LightGreen] *Trabajar en equipo
						*[#LightGreen] *Ofrecer honestidad y sinceridad
					*[#GreenYellow] Por sus actitudes
						*[#LightGreen] *Proactividad
						*[#LightGreen] *Volundtad de mejora
						*[#LightGreen] *Responsabilidad
						
@endmindmap
```

## Consultoría de Software
```plantuml
@startmindmap
*[#PaleVioletRed] Consultoría de Software
	*[#OliveDrab] Actividades realizadas
		*[#SkyBlue] Realizan todas aquellas tareas que ayuda \na mantener la informatica de una empresa
			*[#LightGreen] Desarrollo de software
				*[#SkyBlue] Surge a raiz de la necesidad de un \ncliente referente a su negocio
					*[#GreenYellow] Proceso de desarrollo
						*[#SkyBlue] Consultoría 
							*[#LightGreen] Se recaba información de las \nnecesidades del cliente 
								*[#SkyBlue] Primero: Se consulta con el cliente que \nrequerimientos tiene 
								*[#SkyBlue] Segundo: Estudio de viabilidad
									*[#GreenYellow] Se analiza con el cliente si \nel proyecto es viable o no
										*[#SkyBlue] Responde a las preguntas: 
											*[#LightGreen] *¿Cuál es el coste?  \n*¿Se amortiza el coste? \n*¿tiene sentido la inversión?
						*[#SkyBlue] Diseño funcional
							*[#LightGreen] Una vez se probó ser viable, \nse procede con el diseño funcional
							*[#LightGreen] Se determina
								*[#GreenYellow] Lo primero es definir el \nmodelo de datos a utilizar
								*[#SkyBlue] Tipo de información de entrada
								*[#SkyBlue] Cantidad de procesos contenidos
								*[#SkyBlue] Tipo de información salida, \nes decir la información que va a \nentregar el sistema
									*[#LightGreen] Todo se almacena en una base de datos
							*[#LightGreen] Se realiza un prototipo
						*[#SkyBlue] Diseño técnico
							*[#LightGreen] Aquí se determina el lenguaje a utilizar, \nse crean los procesos
							*[#LightGreen] Se traslada el diseño funcional a un lenguaje de programación
					*[#GreenYellow] Recursos para el desarrollo 
						*[#SkyBlue] Se crea pun proyecto
							*[#LightGreen] Sedetermina un jefe de proyecto
							*[#LightGreen] Se conversa con el cliente
							*[#LightGreen] Se realizan pruebas
			*[#LightGreen] Infraestructura 
				*[#SkyBlue] Esta es fundamental para el \nbuen desarrollo de una empresa
				*[#SkyBlue] Es todo el harware que pueda \nposeer una empresa
					*[#GreenYellow] Computadoras, servidores fisicos, etc
	*[#LightGreen] Servicios de mantenimiento
		*[#SkyBlue] Habilitación de las infraestructuras de comunicaciones 
		*[#SkyBlue] A nivel de harware 
			*[#SkyBlue] Procuran que la infraestructura siempre funcione
			*[#SkyBlue] Tiene especial cuidado con la seguridad
		*[#SkyBlue] A nivel de software 
			*[#GreenYellow] Que las aplicaciones o software creado para las \nempresas sean siempre funcional
	*[#LightGreen] Tendencias en el desarrollo \nde sistemas informáticos	
		*[#SkyBlue] En la infraestructuras
			*[#GreenYellow] Sustitución de servidores por la **nube**
				*[#GreenYellow] Esto mejora la capacidad de almacenamiento
				*[#GreenYellow] Ahorra dinero al no ser necesario invertir \nen mucha infraestructuras 
		*[#SkyBlue] En el manejo de información
			*[#GreenYellow] La aparición de la Big Data
		*[#SkyBlue] Nuevas metodologías
			*[#GreenYellow] Más interactivas
			*[#GreenYellow] Automatización de todo tipo de procesos 
	*[#OliveDrab] AXPE, ejemplo de \nconsultoría de software
		*[#LightGreen] Actividades de la empresa
			*[#SkyBlue] *Servicios de consultoria 
			*[#SkyBlue] *Desarrollo de software avanzado
@endmindmap
```


## Aplicación de la ingeniería de software
```plantuml
@startmindmap
*[#PaleVioletRed] Aplicación de la ingeniería de software
	*[#OliveDrab] Como ejemplo tenemos \na la empresa **Stratesys**
		*[#SkyBlue] Descripción
			*[#LightGreen] Es una empresa de servicios de tecnología 
			*[#LightGreen] Con 15 años de antigüedad con sede en España
			*[#LightGreen] Especializados en soluciones SAP
				*[#SkyBlue] ¿que es SAP?
					*[#GreenYellow] De las siglas Sistemas, Aplicaciones y Productos, \nes un programa utilizado para la correcta administración
		*[#SkyBlue] Metodología 
			*[#LightGreen] La metodología es auditada para \nmantener todas las certificaciones
			*[#LightGreen] Se utiliza una metodología estándar \nen el mercado
				*[#SkyBlue] Primero: Se recolectan los requerimientos \ndel cliente 
					*[#GreenYellow] Lo que necesita el proyecto
					*[#GreenYellow] En qué áreas quiere poner la aplicación
				*[#SkyBlue] Segundo: En una reunión de diferentes \nresponsables se determinan
					*[#GreenYellow] Una visión global de lo que hace la empresa
					*[#GreenYellow] Procesos que la empresa ejecuta a diario
				*[#SkyBlue] Tercero: Se genera el requisito funcional
					*[#GreenYellow] Aquí se detalla lo que se necesita hacer
				*[#SkyBlue] Cuarto: Se genera el análisis funcional
					*[#GreenYellow] Este documento se realiza a partir del requisito funcional
					*[#GreenYellow] Determina como se realizarán las tareas
					*[#GreenYellow] Determina cuanto tiempo se tardará
				*[#SkyBlue] Quinto: Análisis técnico
					*[#GreenYellow] Una vez aprobado el análisis funcional, se realiza el análisis técnico 
					*[#GreenYellow] Se realizan trabajos de programación y parametrización  
				
		*[#SkyBlue] Enfoque de proyecto 
			*[#LightGreen] Gestión del alcance o cambios \nde alcance
				*[#SkyBlue] Toma cuenta los requerimientos solicitados por el cliente \ny los nuevos requisitos, en base a esto se determinan los \ncambios y se amplía o delimita el alcance
				*[#SkyBlue] También toma en cuenta la rentabilidad del proyecto
			*[#LightGreen] Puede ser dividido en \ntres grandes fases
				*[#SkyBlue] Fase de análisis
					*[#GreenYellow] Esta fase consiste en el entendimiento de los \nproblemas y requerimientos a resolver
				*[#SkyBlue] Fase de construcción
					*[#GreenYellow] La fase más importante es la de prototipado
						* **Es importante recalcar que la documentación del codigo** \n**es muy importante para una reutilización de codigo**
				*[#SkyBlue] Fases de pruebas
					*[#GreenYellow] Se realizan las pruebas del producto final
@endmindmap
```

```
Descripción:
Este trabajo fue realizado para la materia de FUNDAMENTOS DE INGIENERIA DE SOFTWARE 

Por el alumno : García Mayoral Josse Luis 19161276

Grupo : ISC

```
[GarciaMayoralJoseLuis  @ GitLab](https://gitlab.com/GarciaMayoralJoseLuis)
